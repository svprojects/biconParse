#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QtWidgets/QFileDialog>
#include <QtCore>
#include <QTextStream>
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

#if defined(Q_OS_WIN)
    #define ENDL QString("\r\n")
#else
    #define ENDL QString("\n")
#endif

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void convert();
    void choosePatterns(bool flag);

private:
    Ui::MainWindow *ui;

    bool twoValue;
    bool enableAlignment;
    int aligmentSize;

    QRegExp  header;
    QRegExp  values;

    QString input_file;
    QString output_file;
    QString output2_file;
    QString split;

    QVector<double> frequency;
    QMap<QString, QHash<double, QStringList> > elements;

    bool generateFile(QString filepath, int index);
    QString parseNameElement(QString line);
    QStringList parseValuesElement(QString line);
};

#endif // MAINWINDOW_H
