#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    enableAlignment = false;
    aligmentSize = 20;

    connect(ui->inButton, &QToolButton::clicked, [=]() {
        QString dir = (ui->inEdit->text() != "") ? ui->inEdit->text() : QDir::currentPath();
        QString filePath = QFileDialog::getOpenFileName(this,
                                                        tr("Выбрать исходный файл"),
                                                        dir,
                                                        tr("Text Files (*.txt)"));
        ui->inEdit->setText(filePath);
    });
    connect(ui->outButton, &QToolButton::clicked, [=]() {
        QString dir = (ui->outEdit->text() != "") ? ui->outEdit->text() : QDir::currentPath();
        QString filePath = QFileDialog::getSaveFileName(this,
                                                        tr("Выбрать целевой файл"),
                                                        dir,
                                                        tr("Text Files (*.txt, *csv)"));
        ui->outEdit->setText(filePath);
    });
    connect(ui->out2Button, &QToolButton::clicked, [=]() {
        QString dir = (ui->out2Edit->text() != "") ? ui->out2Edit->text() : QDir::currentPath();
        QString filePath = QFileDialog::getSaveFileName(this,
                                                        tr("Выбрать целевой файл для второго значения"),
                                                        dir,
                                                        tr("Text Files (*.txt, *csv)"));
        ui->out2Edit->setText(filePath);
    });
    connect(ui->exit, &QAction::triggered, []() {
        exit(0);
    });
    connect(ui->startButton, &QToolButton::clicked, this, &MainWindow::convert);
    connect(ui->version, &QCheckBox::clicked, this, &MainWindow::choosePatterns);
    connect(ui->aligmentFlag, &QCheckBox::clicked, [&]() {
        enableAlignment =ui->aligmentFlag->isChecked();
    });

    choosePatterns(ui->version->isChecked());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::convert()
{
    input_file = ui->inEdit->text();
    output_file = ui->outEdit->text();
    output2_file = ui->out2Edit->text();
    split = (ui->spliterEdit->text() != "") ? ui->spliterEdit->text() : "\t";

    QFile ifile(input_file);
    QString current = "";
    frequency.clear();
    elements.clear();

    if(ifile.open(QIODevice::ReadOnly)) {
        QTextStream in(&ifile);
        while(!in.atEnd())
        {
            QString line = in.readLine();
            if(header.exactMatch(line)) {
                QString name = parseNameElement(line);//line.split(' ',QString::SkipEmptyParts)[3];
                elements.insert(name, QHash<double, QStringList>());
                current = name;
//                qDebug() << "HEADER "<< line;
            }
            if(values.exactMatch(line)) {
//                double freq = QString(line.split(' ',QString::SkipEmptyParts)[0]).toDouble();
                QStringList val = parseValuesElement(line);//line.split(QRegExp("[ \t]"),QString::SkipEmptyParts);
                double freq = val.at(0).toDouble();
                if(!(frequency.contains(freq)))
                    frequency.append(freq);
                if(!(elements[current].contains(freq))) {
                    elements[current].insert(freq, val);
                }
//                qDebug() << "VALUES "<< line;
            }
        }
        //запись
        bool status = true;
        status = (status && generateFile(output_file, 1));
        if(twoValue)
            status = (status && generateFile(output2_file, 2));

        if(status) {
            QMessageBox::information(this,
                                     "Успешно!",
                                     QString("Файл(ы) успешно сгенерирован(ы)"),
                                     QMessageBox::Ok);
        }
        else {
            QMessageBox::critical(this,
                                  "Ошибка!",
                                  QString("Не получается сгенерировать новый файл"),
                                  QMessageBox::Ok);
        }


        ifile.close();
    }
    else
        QMessageBox::critical(this,
                              "Ошибка!",
                              QString("Не получается открыть указанный файл"),
                              QMessageBox::Ok);
}

bool MainWindow::generateFile(QString filepath, int index)
{
    bool status = false;
    QFile ofile(filepath);
    if(ofile.open(QIODevice::WriteOnly)) {
        QTextStream out(&ofile);
        QString headerString = "Freq";
        if(enableAlignment) {
            for(auto& item: elements.keys())
                headerString.append(QString("%1%2").arg(split, item.rightJustified(aligmentSize, ' ')));
        }
        else {
            for(auto& item: elements.keys())
                headerString.append(QString("%1%2").arg(split, item));
        }
        out << headerString << ENDL;

        if(enableAlignment) {
            for(auto& fr: frequency)  {
                out << QString::number(fr);
                for(auto& i: elements.keys()) {
                    QString v = (elements[i].contains(fr)) ?
                                (elements[i][fr].size()>index) ?
                                    elements[i][fr][index].rightJustified(aligmentSize, ' ')
                                    : QString("ERROR2").rightJustified(aligmentSize, ' ')
                                  : QString("ERROR1").rightJustified(aligmentSize, ' ');
                    out << split << v;
                }
                out << ENDL;
            }
        }
        else {
            for(auto& fr: frequency)  {
                out << QString::number(fr);
                for(auto& i: elements.keys()) {
                    QString v = (elements[i].contains(fr)) ?
                                (elements[i][fr].size()>index) ?
                                    elements[i][fr][index]
                                    : "ERROR2"
                                  : "ERROR1";
                    out << split << v;
                }
                out << ENDL;
            }
        }
        ofile.close();
        status = true;
    }
    return status;
}

QString MainWindow::parseNameElement(QString line)
{
    if(twoValue) {
        return line.split(QRegExp("[ \t]\"?"),QString::SkipEmptyParts)[3];
    }
    else {
        return line.split(' ',QString::SkipEmptyParts)[3];
    }
}

QStringList MainWindow::parseValuesElement(QString line)
{
    return line.split(QRegExp("[ \t]"),QString::SkipEmptyParts);
}

void MainWindow::choosePatterns(bool flag)
{
    twoValue = flag;
    ui->out2Button->setEnabled(twoValue);
    ui->out2Button->setVisible(twoValue);

    ui->out2Edit->setEnabled(twoValue);
    ui->out2Edit->setVisible(twoValue);

    header.setPattern(".*Frequency.*");
    values.setPattern(".*[0-9]+.?[0-9]* *[-]?[0-9]+.?[0-9]* *[-]?[0-9]+.?[0-9]*");

//    if(twoValue) {
//        header.setPattern(".*Frequency.*");
//        values.setPattern(".*[0-9]+.?[0-9]* *[-]?[0-9]+.?[0-9]* *[-]?[0-9]+.?[0-9]*");
//    }
//    else {
//        header.setPattern(".*Frequency.*");
//        values.setPattern(".*[0-9]+.?[0-9]* *[-]?[0-9]+.?[0-9]*");
//    }
}
